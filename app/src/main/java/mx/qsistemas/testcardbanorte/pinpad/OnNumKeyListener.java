package mx.qsistemas.testcardbanorte.pinpad;



import android.view.View;

/**
 * 
 * 
 * @author hz
 * @date 20160504
 * @version 1.0.0
 * @function
 * @lastmodify
 */
public interface OnNumKeyListener {
	public void onClick(View view);
}
