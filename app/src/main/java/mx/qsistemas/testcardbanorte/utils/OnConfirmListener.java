package mx.qsistemas.testcardbanorte.utils;

/**
 * Created by liudy on 2017/3/28.
 */

public interface OnConfirmListener {
    void OK();
    void Cancel();
}
