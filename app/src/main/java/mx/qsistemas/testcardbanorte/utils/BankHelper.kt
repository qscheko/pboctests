package mx.qsistemas.testcardbanorte.utils

import android.content.Context
import mx.qsistemas.testcardbanorte.models.BankObject
import java.io.UnsupportedEncodingException
import java.security.AccessControlContext
import java.security.Key
import java.security.NoSuchAlgorithmException
import java.security.spec.AlgorithmParameterSpec
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import java.util.zip.CRC32
import org.json.JSONObject
import org.json.JSONArray

object BankHelper {

    fun getBanorteKey(value: String) : ByteArray?{
        if (value.length == 72){
            val hexStr = value.substring(0, 64)
            return hexStringToByteArray(hexStr)
        }
        return null
    }

    fun getBanorteCRC(value: String) : ByteArray?{
        if (value.length == 72){
            val hexStr = value.substring(64, value.length)
            return hexStringToByteArray(hexStr)
        }
        return null
    }

    fun desedeEncrypt(valueToEncrypt: ByteArray, password: ByteArray, vector: ByteArray) : ByteArray? {
        val encrypted: ByteArray
        try {
            val cipher = Cipher.getInstance("AES/CBC/NoPadding")
            cipher.init(Cipher.ENCRYPT_MODE, makeKey(password), makeIv(vector))
            encrypted = cipher.doFinal(valueToEncrypt)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
        return encrypted
    }

    fun aesDecrypt(valueToDecrypt: ByteArray, password: ByteArray, vector: ByteArray): ByteArray? {
        val decrypted: ByteArray
        try {
            val cipher = Cipher.getInstance("AES/CBC/NoPadding")
            val encrypted = Arrays.copyOfRange(valueToDecrypt, 0, valueToDecrypt.size)
            cipher.init(Cipher.DECRYPT_MODE, makeKey(password), IvParameterSpec(vector))
            decrypted = cipher.doFinal(encrypted)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
        return decrypted
    }

    private fun makeKey(password:ByteArray): Key? {
        try {
            return SecretKeySpec(password, "AES")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        return null
    }

    private fun makeIv(vector:ByteArray): AlgorithmParameterSpec? {
        try {
            return IvParameterSpec(vector)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return null
    }

    fun hexStringToByteArray(s: String): ByteArray {
        val b = ByteArray(s.length / 2)
        for (i in b.indices) {
            val index = i * 2
            val v = Integer.parseInt(s.substring(index, index + 2), 16)
            b[i] = v.toByte()
        }
        return b
    }

    fun crc32ofByte(byteToCheck: ByteArray): ByteArray {
        val crc32 = CRC32()
        crc32.update(byteToCheck)
        return hexStringToByteArray(java.lang.Long.toHexString(crc32.value))
    }

    fun printReceipt(printType:Int, bankObject: BankObject, context: Context){

        val objectprint = JSONObject()
        val currentsb: StringBuilder
        val spos = JSONArray()
        var current: JSONObject

        var content = LinkedHashMap<String, String>()
        content["content-type"] = "txt"
        content["size"] = "3"
        content["content"] = "BANORTE\n"
        content["position"] = "center"
        current = JSONObject(content)
        spos.put(current)

        content = LinkedHashMap()
        content["content-type"] = "txt"
        content["size"] = "2"
        content["content"] = "Venta\n"
        content["position"] = "center"
        current = JSONObject(content)
        spos.put(current)

        content = LinkedHashMap()
        content["content-type"] = "txt"
        content["size"] = "2"
        content["content"] = ""
        content["position"] = "center"
        current = JSONObject(content)
        spos.put(current)
    }

    fun toAscii(s: String): Long {
        val sb = StringBuilder()
        var ascString: String? = null
        val asciiInt: Long
        for (i in 0 until s.length) {
            sb.append(s[i].toInt())
            val c = s[i]
        }
        ascString = sb.toString()
        asciiInt = java.lang.Long.parseLong(ascString)
        return asciiInt
    }

}