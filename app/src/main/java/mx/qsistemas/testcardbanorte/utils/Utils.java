package mx.qsistemas.testcardbanorte.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.qsistemas.testcardbanorte.R;

public class Utils {

    public static void volleyWS(final Context context, String url, final HashMap<String, String> map, RequestQueue postQueue, final Integer serviceCode, final OnVolleyResponse listener){
        try {
            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        listener.onServiceCompleted(object.getBoolean("SUCCESS"), object.getString("ERROR_MESSAGE"), object, serviceCode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onServiceCompleted(false, context.getString(R.string.server_error) + ": " + e.getMessage() , null, serviceCode);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String errmess = "Error desconocido";
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        errmess = "Se supero el tiempo de espera al servidor";
                    } else if (error instanceof AuthFailureError) {
                        errmess = "Error de autenticación con el servidor";
                    } else if (error instanceof ServerError) {
                        errmess = "Error interno del servidor";
                    } else if (error instanceof NetworkError) {
                        errmess = "Se supero el tiempo de espera al servidor";
                    } else if (error instanceof ParseError) {
                        errmess = "Error al parsear la respuesta";
                    }
                    listener.onServiceCompleted(false, context.getString(R.string.server_error) + ": " + errmess , null, serviceCode);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return map;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    return super.parseNetworkResponse(response);
                }

            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            postQueue.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
