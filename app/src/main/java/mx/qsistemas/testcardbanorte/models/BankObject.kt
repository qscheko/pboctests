package mx.qsistemas.testcardbanorte.models

object BankObject {
    var MERCHANT_ID:String = "7455440"
    var USER:String = "a7455440"
    var PASSWORD:String = "quet5440"
    var CMD_TRANS:String = "AUTH"
    var TERMINAL_ID:String = "88888888"
    var AMOUNT:String = ""
    var MODE:String = "RND"
    var CUSTOMER_REF1:String = "006"
    var CUSTOMER_REF2:String = "7"
    var CUSTOMER_REF3:String = "8"
    var CUSTOMER_REF4:String = "9"
    var CUSTOMER_REF5:String = "12345"
    var TRACK2:String = "-"
    var ENTRY_MODE:String = "CHIP"
    var EMV_TAGS:String = ""
    var QPS:String = "0"
    var RESPONSE_LANGUAGE:String = "ES"
}