package mx.qsistemas.testcardbanorte;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basewin.define.InputPBOCInitData;
import com.basewin.define.PBOCOption;
import com.basewin.log.LogUtil;
import com.basewin.services.ServiceManager;
import com.basewin.utils.CUPParam;
import com.basewin.utils.GlobalTransData;
import com.basewin.utils.LoadParamManage;
import com.basewin.utils.TimerCountTools;
import com.pos.sdk.emvcore.PosEmvCoreManager;
import com.pos.sdk.emvcore.PosEmvParam;
import com.pos.sdk.emvcore.PosTermInfo;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import mx.qsistemas.testcardbanorte.utils.BankHelper;
import mx.qsistemas.testcardbanorte.utils.Const;
import mx.qsistemas.testcardbanorte.utils.GlobalData;
import mx.qsistemas.testcardbanorte.utils.ProcessDialog;
import mx.qsistemas.testcardbanorte.utils.StringHelper;
import static java.lang.Math.toIntExact;


public class CobroActivity extends AppCompatActivity {

    private EditText etCobro;
    protected ProcessDialog processdialog = null;
    private GlobalTransData mApp = null;
    public static PosEmvCoreManager mPosEmv = null;
    PosTermInfo termInfo = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cobro);
        ServiceManager.getInstence().init(this);
        LogUtil.openLog();
        Button btnCobro = findViewById(R.id.btnCobro);
        Button btnLogin = findViewById(R.id.btnLogin);
        etCobro = findViewById(R.id.etCobro);

        btnCobro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etCobro.getText().toString().equals("")){
                    try {
                        TimerCountTools tools = new TimerCountTools();
                        tools.start();
                        Intent in = new Intent();
                        in.putExtra(InputPBOCInitData.AMOUNT_FLAG, formatAmount());
                        in.putExtra(InputPBOCInitData.USE_DEVICE_FLAG, InputPBOCInitData.USE_MAG_CARD | InputPBOCInitData.USE_RF_CARD | InputPBOCInitData.USE_IC_CARD);
                        in.putExtra(InputPBOCInitData.TIMEOUT, 30);
                        Integer amount = Integer.valueOf(formatAmount().toString());
                        ServiceManager.getInstence().getPboc().startTransfer(PBOCOption.ONLINE_PAY, in, new onlinePBOCListener(CobroActivity.this, amount.toString(),tools));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(CobroActivity.this, "Ingresa monto", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    boolean bRet = false;
                    boolean iRet = ServiceManager.getInstence().getPinpad().loadMainKeyByArea(1,1,"11111111111111111111111111111111");
                    if (!iRet) {
                        Toast.makeText(CobroActivity.this, "load Main Key error!",Toast.LENGTH_SHORT).show();
                    }
                    iRet = ServiceManager.getInstence().getPinpad().loadPinKeyByArea(1,1,"F40379AB9E0EC533F40379AB9E0EC533", null);
                    if (!iRet) {
                        Toast.makeText(CobroActivity.this, "load Pin Key error!",Toast.LENGTH_SHORT).show();
                    }
                    PosEmvParam posEmvParam = ServiceManager.getInstence().getPboc().getPosTermPara();
                    /*set CurrencyCode*/
                    System.arraycopy(new byte[]{0x04, (byte) 0x84}, 0, posEmvParam.TransCurrCode, 0, 2);
                    /*set CountryCode*/
                    System.arraycopy(new byte[]{0x04, (byte) 0x84}, 0, posEmvParam.CountryCode, 0, 2);
                    ServiceManager.getInstence().getPboc().setPosTermPara(posEmvParam);
                    posEmvParam = ServiceManager.getInstence().getPboc().getPosTermPara();
                    LogUtil.i(getClass(), "posEmvParam:" + posEmvParam.toString());
                    LoadParamManage.getInstance().DeleteAllTerParamFile();
                    /*load AMEX test aids*/
                    for (int i = 0; i < Const.Companion.getAid_data().length; i++) {
                        bRet = ServiceManager.getInstence().getPboc().updateAID(0, Const.Companion.getAid_data()[i]);
                        Log.e("CobroActivity", "download " + i + " aid [" + Const.Companion.getAid_data()[i] + "]" + " bRet = " + bRet);
                    }

                    /*load sdk default aids*/
                    for (int j = 0; j < CUPParam.aid_data.length; j++) {
                        bRet = ServiceManager.getInstence().getPboc().updateAID(0, CUPParam.aid_data[j]);
                        Log.e("CobroActivity", "download " + j + " aid [" + CUPParam.aid_data[j] + "]" + " bRet = " + bRet);
                    }
                    setParameter();
                    GlobalData.getInstance().init(CobroActivity.this);
                    GlobalData.getInstance().setLogin(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setParameter() {

        if (mPosEmv==null) {
            mPosEmv=PosEmvCoreManager.getDefault();
            mPosEmv.EmvEnvInit();
        }

        if (termInfo == null) {
            termInfo = mPosEmv.EmvGetTermInfo();
        }
        if (mApp==null) {
            mApp=GlobalTransData.getInstance();
        }

        termInfo.bForcedOnline = 0;
        termInfo.bTermEcSpt = 0;
        termInfo.bSurportPSESel = 1;
        termInfo.bBypassPIN = 1;
        termInfo.bHolderConfirm = 1;
        termInfo.bTransType = PosEmvCoreManager.EMV_GOODS;
        mPosEmv.EmvSetTermInfo(termInfo);
        LogUtil.si(getClass(), "The terminal parameters100000:" + mPosEmv.EmvGetTermInfo().toString());

        PosEmvParam termParam = mPosEmv.EmvGetTermPara();
        termParam.TerminalType= 0x22;
        termParam.ExCapability[3] |= 0x40;
        termParam.Capability[0] = (byte) 0xe0;
        termParam.Capability[1] = (byte) 0xa9;
        termParam.Capability[2] = (byte) 0xc8;
        System.arraycopy(new byte[]{0x05,0x66}, 0, termParam.TransCurrCode, 0, 2);
        System.arraycopy(new byte[]{0x05,0x66}, 0, termParam.CountryCode, 0, 2);
        String name = mApp.mTerm.getMerchantName();
        try {
            byte nameStr[] = name.getBytes("gb2312");
            Arrays.fill(termParam.MerchName, (byte) 0);
            System.arraycopy(nameStr, 0, termParam.MerchName, 0, nameStr.length);
            mPosEmv.EmvSetTermPara(termParam);
            LogUtil.si(getClass(), "Emv kernel parameters:" + mPosEmv.EmvGetTermPara().toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public Long formatAmount() {
        return Long.parseLong(StringHelper.changeAmout(this.etCobro.getText().toString()).replace(".", ""));
    }

    public void freshProcessDialog(final String title) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showProcessDialog(title);
            }
        });
    }

    public void dismissDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProcessDialog();
            }
        });
    }



    public void resultTransaction(final boolean result, final String message, final boolean needsFirma, final String pbocData, final String track2) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result){
                    StringBuilder sb = new StringBuilder();
                    sb.append(message).append(", ");
                    if(needsFirma) {
                        sb.append("NECESITA FIRMA AUTOGRAFA");
                    } else {
                        sb.append("AUTORIZADA CON PINPAD");
                    }
                    Log.d("PBOC", sb.toString());
                    Log.d("PBOC DATA DATA", pbocData);
                    Log.d("PBOC DATA DATA", track2);
                    Toast.makeText(CobroActivity.this, sb.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("PBOC","Error");
                    Toast.makeText(CobroActivity.this, "test", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void dismissProcessDialog() {
        if (processdialog != null && processdialog.isShowing()) {
            processdialog.stopTimer();
            processdialog.dismiss();
            processdialog = null;
        }
    }

    public void showProcessDialog(String title) {
        if (processdialog == null)
            processdialog = new ProcessDialog(this,title);
        else
        {
            processdialog.freshTitle(title);
        }
    }

}
