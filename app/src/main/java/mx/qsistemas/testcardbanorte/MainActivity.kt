package mx.qsistemas.testcardbanorte

import android.annotation.SuppressLint
import android.content.Intent
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import com.android.volley.ParseError
import com.android.volley.toolbox.HttpHeaderParser
import mx.qsistemas.testcardbanorte.utils.BankHelper
import java.io.UnsupportedEncodingException
import java.util.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var postQueue: RequestQueue

    private var cardtype = 0
    private var cardno: String? = null

    private val pinDialogShow = 1      //display pinpad(Pinpad弹出)
    private var ammount:Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        startView()
    }

    private fun startView() {
        postQueue = Volley.newRequestQueue(this)
        btnPost.setOnClickListener(this)
        btnpinpad.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnPost -> {
                val stringRequest = object : StringRequest(Request.Method.POST, "https://via.banorte.com/InterredesSeguro",
                        Response.Listener<String> { response ->
                            try {
                                Toast.makeText(applicationContext, "$response Success", Toast.LENGTH_LONG).show()
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { volleyError ->
                            tvPost.text = volleyError.toString()
                            Toast.makeText(applicationContext, volleyError.message, Toast.LENGTH_LONG).show()
                        }) {
                    @Throws(AuthFailureError::class)
                    override fun getParams(): Map<String, String> {
                        val params = HashMap<String, String>()
                        params["CMD_TRANS"] = "GET_KEY"
                        params["MERCHANT_ID"] = "7455440"
                        params["USER"] = "a7455440"
                        params["PASSWORD"] = "quet5440"
                        params["SELECTOR"] = "F40379AB9E0EC533F40379AB9E0EC533383838383838383820202020202020207B75E6D7"
                        params["CONTROL_NUMBER"] = "0"
                        return params
                    }

                    override fun parseNetworkResponse(response: NetworkResponse?): Response<String> {
                        return try {

                            val texto: String = if (response!!.headers["resultado_payw"] == "A") {
                                response.headers["texto"].toString()
                            } else {
                                "error"
                            }

                            val banortekey: ByteArray
                            val banortecrc: ByteArray
                            val banortekeydecrypted: ByteArray
                            val ownkey: ByteArray
                            val ownstartpair: ByteArray
                            val owncrc: ByteArray
                            val tripleDES: ByteArray

                            var success = false

                            if (texto != "error") {
                                banortekey = BankHelper.getBanorteKey(texto)!!
                                banortecrc = BankHelper.getBanorteCRC(texto)!!
                                ownkey = BankHelper.hexStringToByteArray("F40379AB9E0EC533F40379AB9E0EC53338383838383838382020202020202020")
                                ownstartpair = BankHelper.hexStringToByteArray("00000000000000000000000000000000")
                                banortekeydecrypted = BankHelper.aesDecrypt(banortekey, ownkey, ownstartpair)!!
                                owncrc = BankHelper.crc32ofByte(banortekeydecrypted)
                                //tripleDES = BankHelper.desedeEncrypt(banortekeydecrypted, BankHelper.hexStringToByteArray("0000000000000000000000000000000000000000000000000000000000000000"), ownstartpair)!!


                                if (Arrays.equals(banortecrc, owncrc)) {
                                    success = true
                                }

                                this@MainActivity.runOnUiThread {
                                    tvPost.text = success.toString()
                                }

                            } else {
                                this@MainActivity.runOnUiThread {
                                    tvPost.text = "Error en response"
                                }
                            }





                            Response.success(response.headers["resultado_payw"], HttpHeaderParser.parseCacheHeaders(response))


                        } catch (e: UnsupportedEncodingException) {
                            Response.error(ParseError(e))
                        } catch (je: JSONException) {
                            Response.error(ParseError(je))
                        }

                    }
                }

                postQueue.add(stringRequest)
            }

            R.id.btnpinpad -> {
                val intent = Intent(this, CobroActivity::class.java)
                startActivity(intent)
            }
        }
    }


}
